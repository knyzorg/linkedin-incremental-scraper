const fetch = require('node-fetch');
require('dotenv').config();

const cookie = process.env.COOKIE;
const csrf = process.env.CSRF;
const rateLimit = process.env.RATE;

const timeout = () => new Promise((res) => setTimeout(res, rateLimit));



async function getConversations(createdBefore) {
  const { included: conversationData } = await fetch(
    'https://www.linkedin.com/voyager/api/messaging/conversations?createdBefore=' +
      createdBefore,
    {
      credentials: 'include',
      headers: {
        'User-Agent':
          'Mozilla/5.0 (Windows  NT 10.0; Win64; x64; rv:87.0) Gecko/20100101 Firefox/87.0',
        Accept: 'application/vnd.linkedin.normalized+json+2.1',
        'csrf-token': csrf,
        Cookie: cookie,
      },
    }
  ).then((res) => res.json());
  const conversationUrn = conversationData.filter(
    (c) => c.$type == 'com.linkedin.voyager.messaging.Conversation'
  );
  const miniProfileUrn = conversationData.filter(
    (c) => c.$type == 'com.linkedin.voyager.identity.shared.MiniProfile'
  );

  const miniProfileMap = new Map();
  for (let profile of miniProfileUrn) {
    miniProfileMap.set(profile.entityUrn, profile);
  }

  const eventUrn = conversationData.filter(
    (c) => c.$type == 'com.linkedin.voyager.messaging.Event'
  );
  const eventMap = new Map();
  for (let event of eventUrn) {
    const tmp = eventMap.get(event['*from']);
    if (tmp) tmp.push(event);
    else eventMap.set(event['*from'], [event]);
  }

  const messagingMemberUrn = conversationData.filter(
    (c) => c.$type == 'com.linkedin.voyager.messaging.MessagingMember'
  );
  const messagingMemberMap = new Map();
  for (let messagingMember of messagingMemberUrn) {
    messagingMemberMap.set(messagingMember.entityUrn, messagingMember);
  }

  const invitationUrn = conversationData.filter(
    (c) => c.$type == 'com.linkedin.voyager.relationships.invitation.Invitation'
  );
  const invitationMap = new Map();
  for (let invitation of invitationUrn) {
    invitationMap.set(invitation.entityUrn, invitation);
  }

  console.log(eventMap);
  const data = conversationUrn.map((conversation) => {
    const messagingMembers = conversation['*participants'].map((p) =>
      messagingMemberMap.get(p)
    );
    const miniProfiles = messagingMembers.map((mm) =>
      miniProfileMap.get(mm['*miniProfile'])
    );
    const lastEvents = messagingMembers.flatMap((mm) =>
      eventMap.get(mm.entityUrn)
    );
    return {
      conversation,
      messagingMembers,
      miniProfiles,
      lastEvents,
    };
  });
  const oldestActivity = Math.min(
    ...data.map((d) => d.conversation.lastActivityAt)
  );
  // console.log(data.map(d => d.conversation));
  data
    .sort(
      (a, b) => a.conversation.lastActivityAt - b.conversation.lastActivityAt
    )
    .reverse();
  return { oldestActivity, data };
}

async function getMessages(conversationId, createdBefore) {
  const { included: conversationData } = await fetch(
    'https://www.linkedin.com/voyager/api/messaging/conversations/' +
      conversationId +
      '/events?createdBefore=' +
      createdBefore,
    {
      credentials: 'include',
      headers: {
        'User-Agent':
          'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:87.0) Gecko/20100101 Firefox/87.0',
        Accept: 'application/vnd.linkedin.normalized+json+2.1',
        'csrf-token': csrf,
        Cookie: cookie,
      },
    }
  ).then((res) => res.json());
  const eventUrn = conversationData.filter(
    (c) => c.$type == 'com.linkedin.voyager.messaging.Event'
  );
  // console.log("https://www.linkedin.com/voyager/api/messaging/conversations/" + conversationId + "/events?createdBefore=" + createdBefore);
  const oldestActivity = Math.min(...eventUrn.map((d) => d.createdAt));

  eventUrn.sort((a, b) => a.createdAt - b.createdAt).reverse();
  return { oldestActivity, events: eventUrn };
}
async function* conversationGenerator(oldestMessage) {
  let date = Date.now();
  let conversations = await getConversations(date);
  console.log(oldestMessage, conversations.oldestActivity);
  while (
    conversations.data.length &&
    oldestMessage <= conversations.oldestActivity
  ) {
    for (let d of conversations.data) yield d;
    await timeout();
    conversations = await getConversations(conversations.oldestActivity);
  }
}

async function* messageGenerator(conversationId, oldestMessage) {
  let date = Date.now();
  let messages = await getMessages(conversationId, date);
  while (messages.events.length && oldestMessage <= messages.oldestActivity) {
    for (let d of messages.events) yield d;
    await timeout();
    messages = await getMessages(conversationId, messages.oldestActivity);
  }
}

async function mainF(oldestMessage) {
  for await (let d of conversationGenerator(oldestMessage)) {
    console.log('=> Conversation ID', d.conversation.entityUrn);
    console.log(
      'Message with',
      d.miniProfiles.map((mp) => `${mp.firstName} ${mp.lastName}`)
    );
    console.log(
      'Last event',
      d.lastEvents.map((e) => e.eventContent.attributedBody)
    );
    console.log('Listing all messages...');
    for await (let m of messageGenerator(
      d.conversation.entityUrn.replace('urn:li:fs_conversation:', ''),
      oldestMessage
    )) {
      console.log(m.eventContent);
    }
  }
}

const oldestMessage = 1615309136; //Date.now();
mainF(oldestMessage);
